let firstNameInput = document.querySelector('#txt-first-name')
let lastNameInput = document.querySelector('#txt-last-name')
let h3FullName = document.querySelector('#full-name')
let submitBtn = document.querySelector('#submit-btn')

function showFullName (event){

	h3FullName.innerHTML = '${firstnameInput.value} ${lastNameInput.value}'
}

function submitfullName () {

	console.log(firstnameInput.value === undefined)

	if(firstnameInput.value !== "" && lastNameInput.value !== ""){
		alert('Thank you for registering ${firstnameInput.value} ${lastNameInput.value}')
		firstnameInput.value = ""
		lastNameInput.value = ""
		h3FullName.innerHTML = ""

		window.location.href = "https://www.nba.com"
	} else {
		alert('Please input firstname and lastname')
	}
}

function resetAll (){

	firstnameInput.value = ""
	lastNameInput.value = ""
	h3FullName.innerHTML = ""
}

firstnameInput.addEventListener('keyup', showFullname)
lastNameInput.addEventListener('keyup', showFullname)

submitBtn.addEventListener('click',submitfullName)
resetBtn.addEventListener('click', resetAll)