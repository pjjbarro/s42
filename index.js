/*


In such a case, our elements as objects, we can then access and manipulate the properties of an element. 

We can actually do a lot of things with JS DOM, in fact you've seen a sample before wherein when a button is clicked the style of an element changed. For our JS DOM sessions we will focus mainly on the use of Forms.

*/
console.log(document);
//document referes to the whole page.
//.querySelector() is a method that can be used to select a specific element from our document. The querySelector uses CSS like selectors to select an element.

let firstNameLabel = document.querySelector('#label-first-name');

//We were able to select an element by its id from our document and saved it in a variable.
console.log(firstNameLabel);

//.innerHTML is a property of an element which considers all the children of the selected element as string
console.log(firstNameLabel.innerHTML);
firstNameLabel.innerHTML = "I like New York City."

//Mini-Activity
let lastNameLabel =document.querySelector('#label-last-name');
lastNameLabel.innerHTML = "My favorite food is Spaghetti."

let city = "Greece";

//Set a condition wherein if the city variable is not New York, we will show the value otherwise:

if(city === 'New York'){

	firstNameLabel.innerHTML = 'I Like ${New York} City'

} else {

	firstNameLabel.innerHTML = 'I dont like New York. I like ${city} city'
}